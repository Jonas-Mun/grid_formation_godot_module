#include "grid_formation_3d.h"

void GridFormation3D::new_positions(Vector3 i_pos, Vector3 e_pos) {
    printf("Received I Pos: %f %f %f", i_pos.x, i_pos.y, i_pos.z);

    Vector<Vector3> new_pos = GFBrain3D::general_positions(i_pos, e_pos, size_of_unit, gap, max_units);

    positions.clear();
    positions = new_pos;    
}

/*
 *
 */
Vector<Vector3> GridFormation3D::get_positions() {
    return positions;
}

void GridFormation3D::set_max_units(int val) {
    max_units = val;
}

int GridFormation3D::get_max_units() {
    return max_units;
}

void GridFormation3D::set_size_of_unit(float val) {
    size_of_unit = val;
}

float GridFormation3D::get_size_of_unit() {
    return size_of_unit;
}

void GridFormation3D::set_gap(float val) {
    gap = val;
}

float GridFormation3D::get_gap() {
    return gap;
}

void GridFormation3D::_bind_methods() {
    ClassDB::bind_method(D_METHOD("new_positions", "i", "e"), &GridFormation3D::new_positions);
    ClassDB::bind_method(D_METHOD("get_positions"), &GridFormation3D::get_positions);
    ClassDB::bind_method(D_METHOD("set_max_units", "val"), &GridFormation3D::set_max_units);
    ClassDB::bind_method(D_METHOD("get_max_units"), &GridFormation3D::get_max_units);
    ClassDB::bind_method(D_METHOD("set_size_of_unit", "val"), &GridFormation3D::set_size_of_unit);
    ClassDB::bind_method(D_METHOD("get_size_of_unit"), &GridFormation3D::get_size_of_unit);
    ClassDB::bind_method(D_METHOD("set_gap", "val"), &GridFormation3D::set_gap);
    ClassDB::bind_method(D_METHOD("get_gap"), &GridFormation3D::get_gap);

    ADD_PROPERTY(PropertyInfo(Variant::INT, "max_units"), "set_max_units", "get_max_units");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "size_of_unit"), "set_size_of_unit", "get_size_of_unit");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "gap"), "set_gap", "get_gap");

}

GridFormation3D::GridFormation3D() {
    max_units = 0;
    size_of_unit = 0;
    gap = 0;
}
