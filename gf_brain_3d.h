#ifndef GFBRAIN_3D_H
#define GFBRAIN_3D_H

#include "core/reference.h"
#include "core/math/math_funcs.h"

#include "core/math/vector3.h"

#include "core/vector.h"

/*
 * Y-axis si the UP Vector
 *
 * X-axis: right vector
 * Z-axis: forward vector
 *
 * X-axis and Z-axis make up the plane.
 *
 */

// Contain the spherical coordinates
struct SphereCoordinates {
    float theta;
    float phi;
};

class GFBrain3D : public Reference {
    GDCLASS(GFBrain3D, Reference);

protected:
    static void _bin_methods();

// Equations
private:
    static int total_units_on_rho(float radius, float dist_btxt);
    static int total_depths(int max_units, int units_on_radius);
    static Vector3 position_on_rho(Vector3 i_pos, float theta, float phi, float dist_btxt, int index);
    static Vector3 depth_unit_position(Vector3 i_pos, float phi, float dist_btxt);

// Procedures
public: 
    static Vector<Vector3> general_positions(Vector3 i_pos, Vector3 e_pos, float size_of_unit, float gap, int max_units);

};
#endif
