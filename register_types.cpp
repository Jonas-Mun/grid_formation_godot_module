#include "register_types.h"

#include "core/class_db.h"
#include "gf_brain.h"
#include "grid_formation.h"
#include "grid_formation_3d.h"

void register_grid_formation_types() {
    ClassDB::register_class<GFBrain>();
    ClassDB::register_class<GridFormation>();
    ClassDB::register_class<GridFormation3D>();
}

void unregister_grid_formation_types() {
    
}
