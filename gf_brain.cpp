#include "gf_brain.h"

int GFBrain::total_units(float radius, float dist_btxt) {
    float units_on_r = radius / dist_btxt;
    return units_on_r + 1;    // take into account the unit on I
}

int GFBrain::total_depths(int max_units, int units_on_radius) {
    int full_depths = max_units / units_on_radius;
    full_depths -= 1;	// treat initial depth as 0th 

    int leftover_depth = max_units % units_on_radius > 0 ? 1 : 0;

    return full_depths + leftover_depth;
}

Vector2 GFBrain::position_on_radius(Vector2 i_pos, float dist_btxt, float angle, int index) {
    float x = index * cos(angle);
    float y = index * sin(angle);

    float offset_x = dist_btxt * x;
    float offset_y = dist_btxt * y;

    float on_radius_x = i_pos.x + offset_x;
    float on_radius_y = i_pos.y + offset_y;

    return Vector2(on_radius_x, on_radius_y);
}

Vector2 GFBrain::depth_unit_position(Vector2 f_pos, float angle, float dist_btxt) {
    // Have x be negative to accomoddate Godot's coordinate system.
    float offset_x = dist_btxt * -sin(angle);

    // Have y be positive to accomodate for Godot's coordinate system.
    float offset_y = dist_btxt * cos(angle);

    float on_depth_x = f_pos.x + offset_x;
    float on_depth_y = f_pos.y + offset_y; 

    return Vector2(on_depth_x, on_depth_y);
}

void GFBrain::_bind_methods() {
    //ClassDB::bind_method(D_METHOD("total_units", "val_0", "val_1"), &GFBrain::total_units);
    //ClassDB::bind_method(D_METHOD("total_depths", "max_units", "units_on_radius"), &GFBrain::total_depths);
}

/* Procedures */

Vector<Vector2> GFBrain::general_positions(Vector2 i_pos, Vector2 e_pos, float size_of_units, float gap, int max_units, float angle) {
    Vector2 radius_v = e_pos - i_pos;
    float radius_length = radius_v.length();
    float dist_btxt = size_of_units + gap;

    int units_on_radius = total_units(radius_length, dist_btxt);
    int total_depths = GFBrain::total_depths(max_units, units_on_radius);

    // Algorithm
    Vector2 cur_i = i_pos;

    Vector<Vector2> positions = Vector<Vector2>();
    //positions.resize(max_units);
    printf("Size of new vectors: %d\n", positions.size());
    
    for(int cur_depth = 0, num_units = 0; cur_depth <= total_depths; cur_depth++) {
        for(int i = 0; i < units_on_radius && num_units < max_units; i++, num_units++) {
            Vector2 position = position_on_radius(cur_i, dist_btxt, angle, i);
	    positions.push_back(position);
	    printf("%d\n",max_units);
	}
	cur_i = depth_unit_position(cur_i, angle, dist_btxt);    // go down to i dpeth
    }

    return positions;

}

GFBrain::GFBrain() {

}
