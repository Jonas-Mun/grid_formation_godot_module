
#include "grid_formation.h"

void GridFormation::new_positions(Vector2 i_pos, Vector2 e_pos) {
    Vector2 v = e_pos - i_pos;
    float angle = right_vector.angle_to(v);
    Vector<Vector2> new_pos = GFBrain::general_positions(i_pos, e_pos, size_of_unit, gap, max_units, angle);
    positions.clear();
    positions = new_pos;
}	

Vector<Vector2> GridFormation::get_positions() {
    return positions;
}

void GridFormation::set_max_units(int val) {
    max_units = val;
}

int GridFormation::get_max_units() {
    return max_units;
}

void GridFormation::set_size_of_unit(float val) {
    size_of_unit = val;
}

float GridFormation::get_size_of_unit() {
    return size_of_unit;
}

void GridFormation::set_gap(float val) {
    gap = val;
}

float GridFormation::get_gap() {
    return gap;
}

void GridFormation::_bind_methods() {
    ClassDB::bind_method(D_METHOD("new_positions", "i", "e"), &GridFormation::new_positions); 
    ClassDB::bind_method(D_METHOD("get_positions"), &GridFormation::get_positions);
    ClassDB::bind_method(D_METHOD("set_max_units", "val"), &GridFormation::set_max_units);
    ClassDB::bind_method(D_METHOD("get_max_units"), &GridFormation::get_max_units);
    ClassDB::bind_method(D_METHOD("set_size_of_unit", "val"), &GridFormation::set_size_of_unit);
    ClassDB::bind_method(D_METHOD("get_size_of_unit"), &GridFormation::get_size_of_unit);
    ClassDB::bind_method(D_METHOD("set_gap", "val"), &GridFormation::set_gap);
    ClassDB::bind_method(D_METHOD("get_gap"), &GridFormation::get_gap);

    ADD_PROPERTY(PropertyInfo(Variant::INT, "max_units"), "set_max_units", "get_max_units");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "size_of_unit"), "set_size_of_unit", "get_size_of_unit");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "gap"), "set_gap", "get_gap");
}

GridFormation::GridFormation() {
   max_units = 0;
   size_of_unit = 0;
   gap = 0; 
   right_vector = Vector2(1,0);	// towards x-axis
}
