#ifndef GRID_FORMATION_H
#define GRID_FORMATION_H

#include "scene/2d/node_2d.h"

#include "core/math/vector2.h"
#include "core/vector.h"
#include "core/array.h"
#include "core/variant.h"

#include "gf_brain.h"

class GridFormation : public Node2D {
    GDCLASS(GridFormation, Node2D);

    int max_units;
    float size_of_unit;
    float gap;
    Vector2 right_vector;	// guide to calculate angles

protected: 
    static void _bind_methods();
    Vector<Vector2> positions;

public:
    
    void set_max_units(int val);
    int get_max_units();

    void set_size_of_unit(float val);
    float get_size_of_unit();

    void set_gap(float gap);
    float get_gap();

    void new_positions(Vector2 i_pos, Vector2 e_pos);
    Vector<Vector2> get_positions();

    GridFormation();

};

#endif
