#ifndef GRID_FORMATION_3D_H
#define GRID_FORMATION_3D_H

#include "scene/3d/spatial.h"

#include "core/math/vector3.h"
#include "core/vector.h"
#include "core/variant.h"

#include "gf_brain_3d.h"

class GridFormation3D : public Spatial {
    GDCLASS(GridFormation3D, Spatial);

    int max_units;
    float size_of_unit;
    float gap;

protected:
    static void _bind_methods();
    Vector<Vector3> positions;

public:
    void set_max_units(int val);
    int get_max_units();

    void set_size_of_unit(float val);
    float get_size_of_unit();

    void set_gap(float gap);
    float get_gap();

    void new_positions(Vector3 i_pos, Vector3 e_pos);
    Vector<Vector3> get_positions();

    GridFormation3D();
};
#endif
