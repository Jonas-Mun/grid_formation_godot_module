#ifndef GFBRAIN_H
#define GFBRAIN_H

#include "core/reference.h"
#include "core/math/math_funcs.h"

#include "core/math/vector2.h"
#include "core/array.h"

#include "core/vector.h"

class GFBrain : public Reference {
    GDCLASS(GFBrain, Reference);

protected:
    static void _bind_methods();

// Equations
private: 
    static int total_units(float radius, float dist_btxt);
    static int total_depths(int max_units, int units_on_radius);
    static Vector2 position_on_radius(Vector2 i_pos, float dist_btxt, float angle, int index);
    static Vector2 depth_unit_position(Vector2 f_pos, float angle, float d);

// Procedures
public:
    static Vector<Vector2> general_positions(Vector2 i_pos, Vector2 e_pos, float size_of_unit, float gap, int max_units, float angle); 

    GFBrain();

};
#endif
