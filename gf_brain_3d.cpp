#include "gf_brain_3d.h"

int GFBrain3D::total_units_on_rho(float radius, float dist_btxt) {
    int total_units = radius / dist_btxt;
    return total_units + 1;	// take into account the unit on I
}

int GFBrain3D::total_depths(int max_units, int units_on_rho) {
    int full_depths = max_units / units_on_rho;
    full_depths -= 1;	// treat initial depth as 0th

    int leftover_depth = max_units % units_on_rho > 0 ? 1 : 0;

    return full_depths + leftover_depth;
}

/** 
 * z - forwad vector
 * x - right vector
 * y - up vector
 */

Vector3 GFBrain3D::position_on_rho(Vector3 i_pos, float theta, float phi, float dist_btxt, int index) {
    float rho_len = dist_btxt * index;

    float x = rho_len * sin(theta) * cos(phi);
    float z = rho_len * sin(theta) * sin(phi);

    float y = rho_len * cos(theta);

    Vector3 offset_pos = Vector3(x, y, z);

    Vector3 position = i_pos + offset_pos;

    return position;
}

/*
 * x-axis: right vector
 * z-axis: forward vector
 */

Vector3 GFBrain3D::depth_unit_position(Vector3 f_pos, float phi, float dist_btxt) {
    float offset_x = -sin(phi) * dist_btxt;	// be negative to go right
    float offset_z = cos(phi) * dist_btxt;	// be positive to go right


    Vector3 depth_position = Vector3(f_pos.x + offset_x, f_pos.y, offset_z + f_pos.z);

    return depth_position;
}

Vector<Vector3> GFBrain3D::general_positions(Vector3 i_pos, Vector3 e_pos, float size_of_unit, float gap, int max_units) {
    Vector3 v = e_pos - i_pos;	// NOT being normalized

    float rho_length = v.length();
    
    // Have at least one unit on I_pos
    if (rho_length == 0) {
        rho_length = size_of_unit;
    }

    float dist_btxt = size_of_unit + gap;
    int units_on_rho = total_units_on_rho(rho_length, dist_btxt);

    // spherical coordinates
    
    float theta = acos(v.y/rho_length);
    float phi = atan2(v.z, v.x);

    int total_depths = GFBrain3D::total_depths(max_units, units_on_rho);

    Vector<Vector3> positions = Vector<Vector3>();
    Vector3 cur_i = i_pos;
    printf("I Pos: %f %f %f\n", i_pos.x, i_pos.y, i_pos.z);

    for(int c_depth = 0; c_depth <= total_depths; c_depth++) {
        // units on current row
        for(int i = 0; i < units_on_rho; i++) {
            Vector3 position = position_on_rho(cur_i, theta, phi, dist_btxt,i);
	    positions.push_back(position);
	}	
	// go to next row
	cur_i = GFBrain3D::depth_unit_position(cur_i, phi, dist_btxt);
    }
    return positions;

}
